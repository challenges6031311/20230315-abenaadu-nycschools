package com.example.nyc_schools.network

import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import com.example.nyc_schools.util.Resource
import javax.inject.Inject

class NycSchoolRepositoryImpl @Inject constructor(
    private val api: NycSchoolsApi
): NycSchoolRepository {
    override suspend fun getNycSchools(): Resource<List<NycSchoolInfo>> {
        val response = try{
            api.getNYCSchools()
        } catch(e: Exception){
            return Resource.Error(e.message ?: "An error occured.")
        }
        return Resource.Success(response)
    }

    override suspend fun getNycSchool(dbn: String): Resource<Array<NycSchoolInfo>> {
        val response = try{
            api.getNYCSchool(dbn)
        } catch(e: Exception){
            return Resource.Error(e.message ?: "An error occured.")
        }
        return Resource.Success(response)
    }

    override suspend fun getSATScores(): Resource<List<SATScoreInfo>> {
        val response = try{
            api.getSATScoreInfos()
        } catch(e: Exception){
            return Resource.Error(e.message ?: "An error occured.")
        }
        return Resource.Success(response)
    }

    override suspend fun getScore(dbn: String): Resource<Array<SATScoreInfo>> {
        val response = try{
            api.getSATScoreInfo(dbn)
        } catch(e: Exception){
            return Resource.Error(e.message ?: "An error occured.")
        }
        return Resource.Success(response)
    }


}