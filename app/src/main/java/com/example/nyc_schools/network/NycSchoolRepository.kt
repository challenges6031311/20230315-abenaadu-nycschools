package com.example.nyc_schools.network

import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import com.example.nyc_schools.util.Resource


interface NycSchoolRepository {
    suspend fun getNycSchools(): Resource<List<NycSchoolInfo>>
    suspend fun getNycSchool(dbn: String): Resource<Array<NycSchoolInfo>>
    suspend fun getSATScores(): Resource<List<SATScoreInfo>>
    suspend fun getScore(dbn: String): Resource<Array<SATScoreInfo>>
}

