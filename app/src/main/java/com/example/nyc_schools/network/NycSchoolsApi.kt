package com.example.nyc_schools.network

import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NycSchoolsApi {

    @GET(NYC_SCHOOLS_PATH)
    suspend fun getNYCSchools(): List<NycSchoolInfo>

    @GET(NYC_SCHOOLS_PATH)
    suspend fun getNYCSchool(
        @Query("dbn") dbn: String
    ): Array<NycSchoolInfo>

    @GET(NYC_SCORES)
    suspend fun getSATScoreInfos(): List<SATScoreInfo>

    @GET(NYC_SCORES)
    suspend fun getSATScoreInfo(
        @Query("dbn") dbn: String
    ): Array<SATScoreInfo>

    companion object{
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
        private const val NYC_SCHOOLS_PATH = "s3k6-pzi2.json"
        private const val NYC_SCORES = "f9bf-2cp4.json"
    }
}