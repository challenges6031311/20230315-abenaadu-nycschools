package com.example.nyc_schools.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NycSchoolApp: Application()