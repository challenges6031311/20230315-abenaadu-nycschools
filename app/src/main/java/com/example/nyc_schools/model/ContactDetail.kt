package com.example.nyc_schools.model

import androidx.compose.ui.graphics.painter.Painter

data class ContactDetail(
    val item: String,
    val painter: Painter,
    val onClickAction: () -> Unit
)