package com.example.nyc_schools.ui.nav

import androidx.navigation.NavType
import androidx.navigation.navArgument
import java.net.URLEncoder

object DetailsRoute {
    const val DBN = "dbn"
    private const val WEBSITE = "website"
    private const val PHONE = "phone"
    private const val LOCATION = "location"
    private const val CITY = "city"
    private const val STATE_CODE = "stateCode"
    const val ROUTE_DETAILS =
        "scores/{$DBN}/{$CITY}/{$STATE_CODE}/{$LOCATION}/{$WEBSITE}/{$PHONE}"

    fun routeDetails(
        dbn: String,
        location: String,
        website: String,
        phone: String,
        city: String,
        stateCode: String
    ) = "scores/$dbn/$city/$stateCode/$location/${URLEncoder.encode(website, "UTF-8")}/$phone"

    val detailsArgument = listOf(DBN,CITY, STATE_CODE, LOCATION, WEBSITE, PHONE).map { arg ->
        navArgument(arg) {
            type = NavType.StringType
            nullable = false
        }
    }
}