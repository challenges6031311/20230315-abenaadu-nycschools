package com.example.nyc_schools.ui.view

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import com.example.nyc_schools.ui.NycSchoolsViewModel
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.nyc_schools.R


@Composable
fun NycSchoolsScreen(
    navController: NavController,
    onClick: (
        dbn: String,
        location: String,
        website: String,
        phone: String,
        city: String,
        stateCode:String
    ) -> Unit,
    viewModel: NycSchoolsViewModel = hiltViewModel()
) {
    val state by viewModel.state.collectAsState()

    if (state.showLoading) {
        ShowProgressBar()
    } else {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            SearchBar(
                hint = stringResource(id = R.string.hint),
                onQueryChange = viewModel::searchSchools
            )
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                state.groupedNycSchoolList.forEach { (first, second) ->
                    item {
                        SectionHeader(text = first.toString())
                    }
                    items(second) { school ->
                        SchoolItem(
                            name = school.schoolName.toString(),
                            onClick = {
                                onClick(
                                    school.dbn.toString(),
                                    school.location.toString(),
                                    school.website.toString(),
                                    school.phoneNumber.toString(),
                                    school.city.toString(),
                                    school.stateCode.toString()
                                )
                            }
                        )
                    }
                }
            }
        }

    }
}

@Composable
fun ShowProgressBar()
{
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(30.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .size(45.dp),
        )
    }
}


@Composable
private fun SchoolItem(
    name: String,
    onClick: () -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onClick()
            },
//        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            style = MaterialTheme.typography.body1.copy(
                color = Color.Black,
                fontWeight = FontWeight.SemiBold
            ),
            text = name,
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(0.8F)
        )
        CustomDivider()
    }

}

@Composable
private fun SectionHeader(
    text: String
){
    Column( modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            style = MaterialTheme.typography.h4.copy(
                color = Color.LightGray
            ),
            text = text,
            modifier = Modifier.padding(horizontal = 10.dp, vertical = 5.dp)
        )
        CustomDivider()
    }
}


@Composable
private fun SearchBar(
    hint: String = "",
    onQueryChange: (String) -> Unit = {}
){
    var text by remember { mutableStateOf("") }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 20.dp)
            .border(
                BorderStroke(1.dp, Color.LightGray),
                shape = RectangleShape
            )
    ) {
        TextField(
            value = text,
            onValueChange = {
                text = it
                onQueryChange(it)
            },
            textStyle = MaterialTheme.typography.body1.copy(
                color = Color.Black,
                fontWeight = FontWeight.Bold
            ),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text
            ),
            singleLine = true,
            maxLines = 1,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 5.dp, vertical = 5.dp),
            placeholder = {
                Text(
                    text = hint,
                    style = MaterialTheme.typography.subtitle1.copy(
                        color = Color.LightGray,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center
                    )
                )
            },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Search,
                    modifier = Modifier
                        .size(32.dp)
                        .padding(start = 5.dp),
                    contentDescription = null,
                    tint = Color.LightGray
                )
            },
            shape = RectangleShape,
            colors = TextFieldDefaults.textFieldColors(
                cursorColor = Color.Black,
                textColor = Color.Black,
                disabledTextColor = Color.Transparent,
                backgroundColor = Color.White,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            )

        )
    }

}

