package com.example.nyc_schools.ui.view

import android.content.Intent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.nyc_schools.R
import com.example.nyc_schools.model.ContactDetail
import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import com.example.nyc_schools.model.ScoreInfo
import com.example.nyc_schools.ui.DetailsViewModel
import com.example.nyc_schools.util.asDialIntent
import com.example.nyc_schools.util.asUriIntent
import com.example.nyc_schools.util.locationIntent

@Composable
fun DetailsScreen (
    navController: NavController,
    viewModel: DetailsViewModel = hiltViewModel(),
    onClick: () -> Unit = {},
) {
    val state by viewModel.state.collectAsState()

    if (state.showLoading) {
        ShowProgressBar()
    } else {
        Column() {
            ExitButton(onClick)
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .verticalScroll(rememberScrollState()),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val schoolInfo = state.schoolInfo
                val score = state.score

                when {
                    schoolInfo.schoolName != null && schoolInfo.city != null && schoolInfo.stateCode != null -> {
                        Header(
                            title = schoolInfo.schoolName,
                            address = " ${schoolInfo.city} , ${schoolInfo.stateCode}",
                            style = MaterialTheme.typography.h6.copy(
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        )
                    }
                }

                ResultsSection(score = score)

                ContactDetails(item = schoolInfo)


            }

        }
    }

}


@Composable
private fun ResultsSection(
    score: SATScoreInfo
){
    Box(
        modifier = Modifier
            .padding(20.dp)
            .fillMaxHeight(0.7F)
            .border(
                BorderStroke(3.dp, Color.LightGray),
                shape = RectangleShape
            ),
        contentAlignment = Alignment.Center

    ) {
        val scores = listOf(
            ScoreInfo(R.string.test_takers, score.numOfSatTestTakers),
            ScoreInfo(R.string.cr_score, score.satCriticalReadingAvgScore),
            ScoreInfo(R.string.maths_score, score.satMathAvgScore),
            ScoreInfo(R.string.writing_score, score.satWritingAvgScore)
        )

        Column {
            scores.forEach { score ->
                RowText(
                    item = stringResource(id = score.labelResId, score.value ?: "")
                )
                CustomDivider()
            }
        }
    }
}

@Composable
private fun ExitButton(
    onClick: () -> Unit
){
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.End
    ) {
        IconButton(onClick = onClick) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = null,
                tint = Color.LightGray,
                modifier = Modifier.size(30.dp)
            )

        }
    }
}

@Composable
private fun Header(
    title: String,
    address: String? = null,
    style: TextStyle
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {

        RowText(
            style = style,
            item = title,
            modifier = Modifier
                .padding( horizontal = 10.dp)
        )

        RowText(
            style = MaterialTheme.typography.subtitle1.copy(
                color = Color.DarkGray,
                fontSize = 18.sp
            ),
            item = address ?: "",
            modifier = Modifier
                .padding(5.dp)
        )

        Divider(
            modifier = Modifier
                .padding(top = 5.dp, bottom = 7.dp)
                .shadow(2.dp),
            color = Color.LightGray,
            thickness = 3.dp
        )
    }

}

@Composable
private fun ContactDetails(
    item: NycSchoolInfo
){
    val context = LocalContext.current
    val contactDetails = listOf(
        ContactDetail(
            item = item.location?.split("(")?.get(0) ?: "",
            painter = painterResource(id = R.drawable.ic_map),
        ) {
            context.startActivity(
                locationIntent(
                    item.latitude ?: "",
                    item.longitude ?: "",
                    item.schoolName ?: ""
                )
            )
        },
        ContactDetail(
            item.website ?: "",
            painterResource(id = R.drawable.ic_website)
        ) {
            context.startActivity((item.website?.asUriIntent() ?: " ") as Intent?)
        },
        ContactDetail(
            item.phoneNumber ?: "",
            painterResource(id = R.drawable.ic_cell)
        ) {
            context.startActivity((item.phoneNumber?.asDialIntent() ?: " ") as Intent?)
        }

    )

    Column {
        RowText(
            item = stringResource(id = R.string.contact_details),
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(10.dp)
        )
        contactDetails.forEach { contactDetail ->
            ContactDetailRow(
                item = contactDetail.item,
                painter = contactDetail.painter
            ) {
                contactDetail.onClickAction()
            }
        }
    }
}


@Composable
private fun ContactDetailRow(
    item: String,
    painter: Painter,
    onClick: () -> Unit = {}
){
    Column (
        modifier = Modifier
            .fillMaxSize()
    ){
        Row(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Icon(
                painter = painter,
                contentDescription = null,
                modifier = Modifier
                    .size(30.dp)
                    .align(Alignment.CenterVertically)
                    .padding(start = 5.dp)

            )

            TextButton(
                onClick = onClick,
                modifier = Modifier
                    .padding(5.dp)
            ) {
                Text (
                    text = item,
                    style = MaterialTheme.typography.body1.copy(
                        color = Color.Blue
                    ),
                    textDecoration = TextDecoration.Underline
                )
            }
        }
    }
}

@Composable
private fun RowText (
    item: String,
    modifier: Modifier = Modifier.padding(16.dp),
    style: TextStyle = MaterialTheme.typography.h6.copy(color = Color.Black),
) {
    Row(
        modifier = modifier
    ) {
        Text(
            style = style,
            text = item
        )

    }
}


