package com.example.nyc_schools

import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import com.example.nyc_schools.network.NycSchoolsApi
import com.example.nyc_schools.util.Resource
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class NycSchoolApiTest {

    // Create a test version of NycSchoolsService
    private val mockSchoolsService = mockk<NycSchoolsApi>()


    // Return mocked response for school's API
    @Test
    fun ` return school data from response `() = runBlocking {
        // Set up mocked response
        val mockResponse = listOf(NycSchoolInfo("School A"), NycSchoolInfo("School B"))
        coEvery { mockSchoolsService.getNYCSchools() } returns mockResponse

        // Call the method being tested
        val response = mockSchoolsService.getNYCSchools()

        // Verify the response
        assertEquals(false, response.isEmpty())
        assertEquals(Resource.Success(response).data, response)
    }

    // Return mocked response for score's API
    @Test
    fun `return SAT score data from response`() = runBlocking{
        // Set up mocked response
        val mockResponse = listOf(SATScoreInfo("Score A"), SATScoreInfo("Score B"))
        coEvery { mockSchoolsService.getSATScoreInfos() } returns mockResponse

        // Call the method being tested
        val response = mockSchoolsService.getSATScoreInfos()

        // Verify the response
        assertEquals(false, response.isEmpty())
        assertEquals(Resource.Success(response).data, response)
    }


}