package com.example.nyc_schools

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import com.example.nyc_schools.model.NycSchoolInfo
import com.example.nyc_schools.model.SATScoreInfo
import com.example.nyc_schools.network.NycSchoolRepository
import com.example.nyc_schools.ui.*
import com.example.nyc_schools.util.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DetailsViewModelTest {

    // Use coroutineTestRule to handle the ViewModelScope
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private lateinit var viewModel: DetailsViewModel
    private lateinit var repository: NycSchoolRepository

    @Before
    fun setUp() {
        // Initialize the DetailsViewModel with a mocked repository and a SavedStateHandle
        repository = mock(NycSchoolRepository::class.java)
        viewModel = DetailsViewModel(repository, SavedStateHandle())
    }



    @Test
    fun `when getData is called with a valid dbn, state should be updated with success result`() =
        runBlocking {
            // Given a valid dbn and successful repository responses
            val dbn = "123456"
            val schoolResult = Resource.Success(arrayOf(NycSchoolInfo(dbn)))
            val scoreResult = Resource.Success(arrayOf(SATScoreInfo(dbn)))
            `when`(repository.getNycSchool(dbn)).thenReturn(schoolResult)
            `when`(repository.getScore(dbn)).thenReturn(scoreResult)

            // When calling getData
            viewModel.getData(dbn)

            // Then state should be updated with the expected values
            val expectedState = DetailsState(
                schoolInfo = schoolResult.data?.toNycSchoolInfo() ?: NycSchoolInfo(),
                score = scoreResult.data?.toSATScoreInfoList() ?: SATScoreInfo(),
                showLoading = false,
                showError = false
            )
            assertEquals(viewModel.state.value, expectedState)
        }

    @Test
    fun `when getData is called with an invalid dbn, state should be updated with error result`() =
        runBlocking {
            // Given an invalid dbn and error repository responses
            val dbn = ""
            val schoolResult = Resource.Error<Array<NycSchoolInfo>>(Exception().message.toString())
            val scoreResult = Resource.Error<Array<SATScoreInfo>>(Exception().message.toString())
            `when`(repository.getNycSchool(dbn)).thenReturn(schoolResult)
            `when`(repository.getScore(dbn)).thenReturn(scoreResult)

            // When calling getData
            viewModel.getData(dbn)

            // Then state should be updated with the expected values
            val expectedState = DetailsState(
                schoolInfo = NycSchoolInfo(),
                score = SATScoreInfo(),
                showLoading = true,
                showError = false
            )
            assertEquals(viewModel.state.value, expectedState)
        }
}
